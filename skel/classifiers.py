from sklearn import tree
import numpy as np
from sklearn.ensemble import RandomForestClassifier


def create_decision_tree_hiring_dataset(df, n_train_percent):
    # separate the feature (input) columns from the target column
    features = list(df.columns)
    features = features[:6]

    print("features: ")
    print(features)

    print("data: ")
    print(df.head())

    # get the target column
    target = df['Hired']
    X = df[features]
    y = target

    # split train/test data set
    # TODO 1: change the number of samples for the training data
    # e.g.
    n_data = len(X)
    n_train = int(n_train_percent * n_data / 100)

    # now actually build the decision tree using the training data set
    clf = tree.DecisionTreeClassifier()
    clf.fit(X[1:n_train], y[1:n_train])

    # predict the test data set using the model
    y_predict = clf.predict(X[n_train + 1:])

    y_predict_a = np.array(y_predict)
    y_test_a = np.array(y[n_train + 1:])
    y_diff_a = y_test_a - y_predict_a

    print("target (test): ")
    print(y_test_a)
    print("prediction (test): ")
    print(y_predict_a)
    print("diff (test): ")
    print(y_diff_a)

    accuracy = len([yd for yd in y_diff_a if yd == 0]) / len(y_diff_a) * 100
    print("accuracy: " + str(round(accuracy, 2)) + " %")

    return clf, features, accuracy


def create_random_forest_hiring_dataset(df, n_train_percent):
    # separate the feature (input) columns from the target column
    features = list(df.columns)
    features = features[:6]

    print("features: ")
    print(features)

    print("data: ")
    print(df.head())

    # get the target column
    target = df['Hired']
    X = df[features]
    y = target

    # split train/test data set
    # TODO 1: change the number of samples for the training data
    # e.g.
    n_data = len(X)
    n_train = int(n_train_percent * n_data / 100)

    # now actually build the decision tree using the training data set
    clf = RandomForestClassifier(n_estimators=10)
    clf.fit(X[1:n_train], y[1:n_train])

    # predict the test data set using the model
    y_predict = clf.predict(X[n_train + 1:])

    y_predict_a = np.array(y_predict)
    y_test_a = np.array(y[n_train + 1:])
    y_diff_a = y_test_a - y_predict_a

    print("target (test): ")
    print(y_test_a)
    print("prediction (test): ")
    print(y_predict_a)
    print("diff (test): ")
    print(y_diff_a)

    accuracy = len([yd for yd in y_diff_a if yd == 0]) / len(y_diff_a) * 100

    return clf, features, accuracy


def create_regression_tree_boston(df, n_train_percent):
    # separate the feature (input) columns from the target column
    features = list(df.columns)
    features = features[:6]

    print("features: ")
    print(features)

    print("data: ")
    print(df)

    # get the target column
    target = df['MEDV']
    X = df[features]
    y = target

    avg = np.mean(y)

    y = [1 if e > avg else 0 for e in y]

    # split train/test data set
    n_data = len(X)
    n_train = int(n_train_percent * n_data / 100)

    # now actually build the decision tree using the training data set
    # clf = tree.DecisionTreeRegressor()

    # TODO 1: compare the decision tree with the regression tree output
    clf = tree.DecisionTreeClassifier()

    clf.fit(X[1:n_train], y[1:n_train])

    # predict the test data set using the model
    y_predict = clf.predict(X[n_train + 1:])

    y_predict_a = np.array(y_predict)
    y_test_a = np.array(y[n_train + 1:])
    y_diff_a = y_test_a - y_predict_a

    print("target (test): ")
    print(y_test_a)
    print("prediction (test): ")
    print(y_predict_a)
    print("diff (test): ")
    print(y_diff_a)

    accuracy = len([yd for yd in y_diff_a if yd == 0]) / len(y_diff_a) * 100
    print("accuracy: " + str(round(accuracy, 2)) + " %")

    return clf, features, accuracy