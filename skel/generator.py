
from sklearn import tree
from print_tree import *
import numpy as np
import pandas as pd
import numpy.random as random


from preprocessing import reverse_mapping

def generate_samples(df, clf):
    features = list(df.columns)
    n_samples = 100

    y_exp = random.random_integers(0, 20, n_samples)
    emp = random.random_integers(0, 1, n_samples)
    prev_emp = random.random_integers(0, 4, n_samples)
    level_ed = random.random_integers(0, 2, n_samples)
    top_school = random.random_integers(0, 1, n_samples)
    interned = random.random_integers(0, 1, n_samples)

    print(features)

    df1 = pd.DataFrame(columns=features)
    for i in range(n_samples):
        # print(new_df)
        d = [[y_exp[i], emp[i], prev_emp[i], level_ed[i], top_school[i], interned[i], 0]]
        new_df = pd.DataFrame(data=d, columns=features)
        # print(d)
        y_predict = clf.predict([d[0][:6]])
        # print(y_predict)
        new_df["Hired"] = y_predict
        # d1 = d.append(y_predict)
        # print(d1)
        # new_df.loc[0] = d1
        # print(new_df)
        df1 = df1.append(new_df, ignore_index=True)

    return df1
    # print(df1)

def create_tree(df):
    # separate the feature (input) columns from the target column
    features = list(df.columns)

    # don't include the target in the features
    features = features[:6]

    print("features: ")
    print(features)

    print("data: ")
    print(df.head())

    # get the target column
    target = df['Hired']
    X = df[features]
    y = target

    # now actually build the decision tree using the training data set
    clf = tree.DecisionTreeClassifier()
    clf.fit(X, y)

    # predict the test data set using the model
    y_predict = clf.predict(X)

    # test the prediction accuracy
    # convert to numpy array for simplicity, get the difference and percent match
    y_a = np.array(y)
    y_predict_a = np.array(y_predict)
    y_diff_a = y_predict_a - y_a

    print("target data: ")
    print(y_a)
    # print(list(y))
    print("predicted data: ")
    print(y_predict_a)
    # print(list(y_predict))

    percent_match = len([yd for yd in y_diff_a if yd == 0]) / len(y_diff_a) * 100
    print("percent match: " + str(percent_match) + "%")
    # note the prediction is 100%
    # the data set is the same though

    # tree.export_graphviz(clf, out_file='tree1.dot', feature_names=features)
    # print_decision_tree(mytree=clf, features=features)

    df = generate_samples(df, clf)
    # print(df)
    reverse_mapping(df)
    print(df)

    df.to_csv("./past_hires_extra.csv", index=False)

    # print(df.head())