import numpy as np
import pandas as pd

# import our modules
import example
import task2
import task3
import generator
from preprocessing import preprocess

# read the data from the csv file
# input_file = "./PastHires.csv"
input_file = "./past_hires.csv"

features = []
df = pd.read_csv(input_file, header=0)

preprocess(df)

print(df)

# TODO: select task
case = 1

if case == 0:
    pass
    # generator.create_tree(df)
elif case == 1:
    example.create_tree(df)
elif case == 2:
    task2.create_tree(df)
elif case == 3:
    task3.create_tree(df)
else:
    print("undefined task")



