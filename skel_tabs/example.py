
from print_tree import *
import numpy as np


def create_tree(df):
	# separate the feature (input) columns from the target column
	features = list(df.columns)
	features = features[:6]

	print("features: ")
	print(features)

	print("data: ")
	# print(df.head())

	print(df)

	# get the target column
	target = df['Hired']
	X = df[features]
	y = target

	# now actually build the decision tree using the training data set
	clf = tree.DecisionTreeClassifier()
	clf.fit(X, y)

	# predict the test data set using the model
	y_predict = clf.predict(X)

	# test the prediction accuracy
	# convert to numpy array for simplicity, get the difference and percent match
	y_a = np.array(y)
	y_predict_a = np.array(y_predict)
	y_diff_a = y_predict_a - y_a

	print("target data: ")
	print(y_a)
	# print(list(y))
	print("predicted data: ")
	print(y_predict_a)
	# print(list(y_predict))

	accuracy = len([yd for yd in y_diff_a if yd == 0]) / len(y_diff_a) * 100
	print("accuracy: " + str(round(accuracy, 2)) + " %")
	# note the prediction is 100%
	# the data set is the same though

	# tree.export_graphviz(clf, out_file='tree1.dot', feature_names=features)
	print_decision_tree(mytree=clf, features=features)
	# print_tree_graph(clf, features)
