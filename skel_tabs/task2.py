
from print_tree import *
import numpy as np
import matplotlib.pyplot as plt
import classifiers


def create_tree(df):
	clf, features, accuracy = classifiers.create_decision_tree_hiring_dataset(df, 80)

	# TODO 1: observe the decision tree that is generated in each case
	print_decision_tree(mytree=clf, features=features)

	# TODO 2: plot the percent match for multiple scenarios
